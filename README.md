**Notes:**

1. You should initialize Mongo cluster from mongo cli like this:
```
var cfg = {
    "_id": "rs0",
    "version": 1,
    "members": [
        {
            "_id": 0,
            "host": "localmongo-primary:27017"
        },
        {
            "_id": 1,
            "host": "localmongo-secondary:27017"
        },
        {
            "_id": 2,
            "host": "localmongo-arbiter:27017"
        }
    ]
};
rs.initiate(cfg, { force: true });
rs.slaveOk();
```

2. Update *namespaces* property in *mongoconnect-image/confi.json* according to your mongo data.
In my example *info* is database name and *data* is collection name.

Start docker:
```
> sudo docker-compose up -d
```
Connect to mongo:
```
> sudo docker container exec -it localmongo-primary /bin/bash
> mongo
```
